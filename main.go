package errors

import (
	"net/http"
	"os"
	"runtime/debug"
	"time"
)

// Error is the main struct for our errors
type Error struct {
	ID     string `json:"id"`
	Code   int32  `json:"code"`
	Detail string `json:"detail"`
	Status string `json:"status"`

	Application string    `json:"application"`
	Hostname    string    `json:"hostname"`
	Timestamp   time.Time `json:"timestamp"`
	Stack       string    `json:"stack"`
}

// New method to create a new error
func New(id, detail string, code int32) *Error {
	hostname, _ := os.Hostname()

	return &Error{
		ID:          id,
		Code:        code,
		Detail:      detail,
		Status:      http.StatusText(int(code)),
		Application: os.Args[0],
		Hostname:    hostname,
		Timestamp:   time.Now(),
		Stack:       string(debug.Stack()),
	}
}
